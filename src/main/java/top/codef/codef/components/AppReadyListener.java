//package top.codef.codef.components;
//
//import java.util.List;
//
//import org.springframework.boot.actuate.health.Health;
//import org.springframework.boot.actuate.health.HealthIndicator;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.cloud.client.serviceregistry.Registration;
//import org.springframework.context.ApplicationListener;
//
//import top.codef.codef.entities.ProjectState;
//import top.codef.codef.entities.enums.ServerState;
//import top.codef.codef.feign.ServiceStateNotice;
//import top.codef.codef.properties.PrometheusCodefCloudProperties;
//
//public class AppReadyListener implements ApplicationListener<ApplicationReadyEvent> {
//
//	private final List<HealthIndicator> healthIndicators;
//
//	private final ServicePreRegistListener servicePreRegistListener;
//
//	private final PrometheusCodefCloudProperties prometheusCodefCloudProperties;
//
//	private final List<ServiceStateNotice> serviceStateNotices;
//
//	/**
//	 * @param healthIndicators
//	 */
//	public AppReadyListener(List<HealthIndicator> healthIndicators, ServicePreRegistListener servicePreRegistListener,
//			PrometheusCodefCloudProperties prometheusCodefCloudProperties,
//			List<ServiceStateNotice> serviceStateNotices) {
//		super();
//		this.healthIndicators = healthIndicators;
//		this.servicePreRegistListener = servicePreRegistListener;
//		this.prometheusCodefCloudProperties = prometheusCodefCloudProperties;
//		this.serviceStateNotices = serviceStateNotices;
//	}
//
//	@Override
//	public void onApplicationEvent(ApplicationReadyEvent event) {
//		String appName = event.getApplicationContext().getEnvironment().getProperty("spring.applciation.name");
//		Health unHealth = healthIndicators.stream().map(x -> x.getHealth(true))
//				.filter(x -> !x.getStatus().getCode().equals("UP")).findAny().orElse(null);
//		Registration registration = servicePreRegistListener.getRegistration();
//		ProjectState projectState = unHealth == null
//				? new ProjectState(appName, registration.getServiceId(), registration.getInstanceId(),
//						prometheusCodefCloudProperties.getTenantId(), ServerState.UP,
//						prometheusCodefCloudProperties.getDeployProcessUid())
//				: new ProjectState(appName, registration.getServiceId(), registration.getInstanceId(),
//						prometheusCodefCloudProperties.getTenantId(), ServerState.state(unHealth),
//						unHealth.getDetails().toString(), prometheusCodefCloudProperties.getDeployProcessUid());
//		serviceStateNotices.forEach(x -> x.notice(projectState));
//	}
//
//}
