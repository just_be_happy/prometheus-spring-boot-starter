package top.codef.microservice.interfaces;

import java.util.Set;

import top.codef.pojos.servicemonitor.MicroServiceReport;
import top.codef.pojos.servicemonitor.ServiceHealthProblem;
import top.codef.pojos.servicemonitor.ServiceInstanceLackProblem;

public interface ServiceNoticeRepository {

	void addServiceLackProblem(ServiceInstanceLackProblem serviceInstanceLackProblem);

	void addServiceHealthProblem(ServiceHealthProblem serviceHealthProblem);

	void addLackServices(Set<String> serviceName);

	void addLackServices(String... serviceName);

	MicroServiceReport report();
}
