package top.codef.microservice.control;

import java.util.concurrent.ScheduledFuture;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.PeriodicTrigger;

import top.codef.microservice.interfaces.ServiceNoticeRepository;
import top.codef.microservice.task.ServiceNoticeTask;
import top.codef.notice.NoticeComponentFactory;
import top.codef.properties.PrometheusProperties;
import top.codef.properties.servicemonitor.ServiceMonitorProperties;
import top.codef.text.NoticeTextResolverProvider;

public class ServiceNoticeControl implements SmartInitializingSingleton, DisposableBean {

	private final Log logger = LogFactory.getLog(ServiceNoticeControl.class);

	private final ServiceMonitorProperties serviceMonitorProperties;

	private final PrometheusProperties promethreusNoticeProperties;

	private final TaskScheduler taskScheduler;

	private final ServiceNoticeRepository serviceNoticeRepository;

	private final NoticeComponentFactory noticeComponentFactory;

	private final NoticeTextResolverProvider noticeTextResolverProvider;

	private ScheduledFuture<?> result;

	/**
	 * @param serviceMonitorProperties
	 * @param promethreusNoticeProperties
	 * @param taskScheduler
	 * @param serviceCheckNoticeRepository
	 * @param noticeSendComponent
	 * @param reportedFilterHandler
	 * @param result
	 */
	public ServiceNoticeControl(ServiceMonitorProperties serviceMonitorProperties,
			PrometheusProperties promethreusNoticeProperties, TaskScheduler taskScheduler,
			ServiceNoticeRepository serviceNoticeRepository, NoticeTextResolverProvider noticeTextResolverProvider,
			NoticeComponentFactory noticeComponentFactory) {
		this.serviceMonitorProperties = serviceMonitorProperties;
		this.promethreusNoticeProperties = promethreusNoticeProperties;
		this.taskScheduler = taskScheduler;
		this.serviceNoticeRepository = serviceNoticeRepository;
		this.noticeComponentFactory = noticeComponentFactory;
		this.noticeTextResolverProvider = noticeTextResolverProvider;
	}

	public ServiceMonitorProperties getServiceMonitorProperties() {
		return serviceMonitorProperties;
	}

	/**
	 * @return the result
	 */
	public ScheduledFuture<?> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(ScheduledFuture<?> result) {
		this.result = result;
	}

	/**
	 * @return the taskScheduler
	 */
	public TaskScheduler getTaskScheduler() {
		return taskScheduler;
	}

	@Override
	public void destroy() throws Exception {
		result.cancel(false);
	}

	@Override
	public void afterSingletonsInstantiated() {
		logger.debug("开启通知任务");
		ServiceNoticeTask serviceNoticeTask = new ServiceNoticeTask(noticeComponentFactory, promethreusNoticeProperties,
				serviceNoticeRepository, noticeTextResolverProvider);
		PeriodicTrigger trigger = new PeriodicTrigger(serviceMonitorProperties.getServiceCheckNoticeInterval());
		trigger.setInitialDelay(serviceMonitorProperties.getServiceNoticeInitialDelay());
		result = taskScheduler.schedule(serviceNoticeTask, trigger);
	}
}
