package top.codef.config.notice;

import org.springframework.core.Ordered;

import top.codef.text.NoticeTextResolverProvider;

public interface NoticeTextResolverCustomer extends Ordered{

	public void custom(NoticeTextResolverProvider resolverProvider);
}
