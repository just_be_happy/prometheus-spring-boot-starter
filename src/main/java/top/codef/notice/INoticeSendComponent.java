package top.codef.notice;

import top.codef.common.interfaces.Unique;
import top.codef.pojos.PromethuesNotice;
import top.codef.text.NoticeTextResolver;

public interface INoticeSendComponent extends Unique {

	void send(PromethuesNotice exceptionNotice, NoticeTextResolver noticeTextResolver);

}
