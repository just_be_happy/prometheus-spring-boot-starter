package top.codef.text;

import top.codef.notice.INoticeSendComponent;
import top.codef.pojos.PromethuesNotice;

public interface NoticeTextResolver {

	public String resolve(PromethuesNotice notice);

	public boolean support(Class<? extends PromethuesNotice> clazz, INoticeSendComponent sendComponent);
}
